package com.devcamp.jbr30.employeerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeerestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeerestapiApplication.class, args);
	}

}
