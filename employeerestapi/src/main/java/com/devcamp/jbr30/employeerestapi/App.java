package com.devcamp.jbr30.employeerestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class App {
    
    @CrossOrigin
    @GetMapping("/employees")
    public ArrayList <Employee> getEmployee(){
        ArrayList <Employee> employeeList = new ArrayList<>();

        Employee employee1 = new Employee(11, "Kim Huong", "Truong", 3000);
        Employee employee2 = new Employee(12, "Minh Duy", "Huynh", 1000);
        Employee employee3 = new Employee(13, "Mit", "Huynh", 100);

        System.out.println("employee1 la: " + employee1);
        System.out.println("employee2 la: " + employee2);
        System.out.println("employee3 la: " + employee3);

        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);

        return employeeList;
    }
}
